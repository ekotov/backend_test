# coding: utf-8
import datetime
import urllib2
from BeautifulSoup import BeautifulSoup
from django.db import models


COMMANDS = (
    u'Бот, дай мне заголовок сайта',
    u'Бот, дай мне H1 с сайта',
    u'Бот, сохрани для меня информацию',
    u'Бот, напомни мне',
    u'Бот, дай мне все варианты заголовков с сайтов',
    u'Бот, дай мне количество выполненных команд',
)


class Chat(models.Model):
    dt_add = models.DateTimeField(verbose_name=u'Время добавления команды',
                                  auto_now_add=True)
    nick = models.CharField(max_length=255, verbose_name=u'Ник оператора')
    command = models.CharField(max_length=3000,
                               verbose_name=u'Комманда боту')
    result = models.TextField(blank=True, null=True,
                              verbose_name=u'Результат выполнения команды')
    dt_start = models.DateTimeField(null=True,
                                    verbose_name=u'Время старта выполнения команды')
    dt_end = models.DateTimeField(null=True,
                                  verbose_name=u'Время завершения выполнения команды')
    error = models.CharField(max_length=3000, blank=True, null=True,
                             verbose_name=u'Ошибка')

    def __unicode__(self):
        return u'{}: {} -> {} = {}'.format(self.dt_add, self.nick, self.command, self.result)

    def is_error(self):
        if self.error:
            return True
        return False

    def get_idx_command(self):
        for idx, cmd in enumerate(COMMANDS):
            if cmd in self.command:
                return idx
        return None

    def get_url(self):
        idx = self.get_idx_command()
        if idx is None:
            return None
        return self.command.replace(COMMANDS[idx], '').lstrip(' ')

    def get_phrase(self):
        s = self.command
        return s[s.find(u'мне ') + 4: s.find(u' через')].encode('utf-8')

    def get_time(self):
        str = self.command
        str = str[str.find(u' через'): ]
        num = [int(s) for s in str.split() if s.isdigit()]
        t = 'm'
        if u'секунд' in self.command:
            t = 's'
        if not len(num):
            return None, t
        return num[0], t

    def do_command(self):
        self.dt_start = datetime.datetime.now()
        idx = self.get_idx_command()
        delta = datetime.timedelta(seconds=0)
        if idx == 0:
            try:
                soup = BeautifulSoup(urllib2.urlopen(self.get_url()))
                result = soup.title.string
                self.result = result
            except Exception as e:
                self.error = e
        elif idx == 1:
            try:
                soup = BeautifulSoup(urllib2.urlopen(self.get_url()))
                result = soup.find('h1').string
                self.result = u'Нет тега h1'
                if result is not None:
                    self.result = result
            except Exception as e:
                self.error = e
        elif idx == 2:
            url = self.get_url()
            try:
                if url == u'':
                    prev_command = Chat.objects.filter(id__lt=self.id).last()
                    if prev_command.get_idx_command() == 0:
                        url = prev_command.get_url()
                        result = urllib2.urlopen(url)
                        self.result = result.read()
                else:
                    self.result = url
            except Exception as e:
                self.error = e

        elif idx == 3:
            try:
                self.result = self.get_phrase()
                num, t = self.get_time()
                if num is not None:
                    mul = 1
                    if t == 'm':
                        mul = 60
                    delta = datetime.timedelta(seconds=num * mul)
            except Exception as e:
                self.error = e

        elif idx == 4:
            try:
                urls = self.get_url().split(', ')
                self.result = u''
                for url in urls:
                    soup = BeautifulSoup(urllib2.urlopen(url))
                    result = soup.title.string
                    if result is None:
                        self.result += u'Нет тега тайтл с сайта {}'.format(url)
                    else:
                        self.result += u'{} с сайта {}'.format(result, url)
            except Exception as e:
                self.error = e
        elif idx == 5:
            self.result = Chat.objects.all().count()

        else:
            self.error = u'Команда не распознана'
        self.dt_end = datetime.datetime.now() + delta
        self.save()
        return self

    class Meta:
        verbose_name = u'Чат'
        verbose_name_plural = u'Чат'
