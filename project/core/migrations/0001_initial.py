# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt_add', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('nick', models.CharField(max_length=255, verbose_name='\u041d\u0438\u043a \u043e\u043f\u0435\u0440\u0430\u0442\u043e\u0440\u0430')),
                ('command', models.CharField(max_length=3000, verbose_name='\u041a\u043e\u043c\u043c\u0430\u043d\u0434\u0430 \u0431\u043e\u0442\u0443')),
                ('result', models.CharField(max_length=3000, null=True, verbose_name='\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b', blank=True)),
                ('dt_start', models.DateTimeField(null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0441\u0442\u0430\u0440\u0442\u0430 \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('dt_end', models.DateTimeField(null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u043a\u043e\u043c\u0430\u043d\u0434\u044b')),
                ('error', models.CharField(max_length=3000, null=True, verbose_name='\u041e\u0448\u0438\u0431\u043a\u0430', blank=True)),
            ],
            options={
                'verbose_name': '\u0427\u0430\u0442',
                'verbose_name_plural': '\u0427\u0430\u0442',
            },
        ),
    ]
