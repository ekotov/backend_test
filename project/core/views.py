# coding: utf-8
import datetime
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse, HttpResponsePermanentRedirect
from django.template.response import TemplateResponse
from core.models import Chat


def home(request):
    if request.method == 'POST':
        nick = request.POST.get('nick', False)
        request.session['nick'] = nick
    if request.session.get('nick', False):
        return HttpResponseRedirect(reverse('chat_view'))
    return TemplateResponse(request, 'login.html')


def chat(request):
    nick = request.session.get('nick', False)
    if not nick:
        return HttpResponseRedirect(reverse('home_view'))
    if request.method == "POST":
        command = request.POST.get('command', None)
        if command is not None:
            res = Chat(nick=nick, command=command)
            res.save()
            return HttpResponseRedirect(reverse('chat_view'))
    res = Chat.objects.filter(dt_end__lte=datetime.datetime.now())
    return TemplateResponse(request, 'chat.html', {'chat': res})