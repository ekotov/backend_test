# coding: utf-8
from time import sleep
import datetime
import urllib2
from django.core.management.base import BaseCommand, CommandError
from core.models import Chat, COMMANDS
from BeautifulSoup import BeautifulSoup


class Command(BaseCommand):
    help = ''

    def handle(self, *args, **options):
        while True:
            commands = Chat.objects.filter(dt_start__isnull=True)[:5]
            for command in commands:
                command.do_command()
            sleep(2)
